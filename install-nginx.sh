#!/bin/bash

#sudo curl https://bitbucket.org/slam121212/ubuntu_laravel_installer/raw/master/install-nginx.sh -o runme.sh && sudo bash runme.sh && sudo rm runme.sh

set -e

USERNAME=username
PASSWORD=password
PROJECTNAME=project

#TIMEZONE="America/Los_Angeles"

#username setup
read -p 'Enter the owner for the project (which EXISTING Ubuntu user will own the project): ' un1
printf "\n"
read -p 'Please repeat the UserName: ' un2
printf "\n"

# Check both passwords match
if [ $un1 != $un2 ]; then
    echo "Usernames do not match. Aborting" && exit 1    
else
    USERNAME=$un1
fi

#password setup
read -s -p 'Choose Password for MySQL root and PHPMyAdmin root: ' pw1
printf "\n"
read -s -p 'Please repeat the password: ' pw2
printf "\n"

# Check both passwords match
if [ $pw1 != $pw2 ]; then
    echo "Passwords do not match. Aborting" && exit 1    
else
    PASSWORD=$pw1
fi

#project setup
read -p 'Choose Project Name(this will also be the name of the projects Database): ' pn1
printf "\n"
read -p 'Please repeat the Project Name: ' pn2
printf "\n"

# Check both passwords match
if [ $pn1 != $pn2 ]; then
    echo "Project Names do not match. Aborting" && exit 1    
else
    PROJECTNAME=$pn1
fi


#set the timezone
sudo dpkg-reconfigure tzdata

#update and upgrade
(sudo apt-get update && sudo apt-get -y upgrade) || (echo "Upgrade Failed. Aborting..." && exit 1)  

#install git
sudo apt-get install -y git-core || (echo "Git Install Failed. Aborting..." && exit 1)

#check if pins exists and delete it, TODO: pull instead of delete
if [ -d /home/$USERNAME/$PROJECTNAME ] 
then 
	sudo echo "project exists. Deleting..."
	sudo rm -R /home/$USERNAME/$PROJECTNAME || (echo "Project Delete Failed. Aborting..." && exit 1) 
fi

#clone repo
git clone https://slam121212@bitbucket.org/slam121212/ubuntu_laravel_installer.git /home/$USERNAME/$PROJECTNAME || (echo "Installer Clone Failed. Aborting..." && exit 1)

sudo chown -R $USERNAME /home/$USERNAME/

#install nginx
sudo apt-get install -y nginx
sudo apt-get install -y php-fpm php
sudo /etc/init.d/nginx stop 
sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.$(date "+%b_%d_%Y_%H.%M.%S") #backup current config file
#Copy new config file to /etc/nginx/niginx.conf
sudo rm /etc/nginx/sites-available/default
sudo cp /home/$USERNAME/$PROJECTNAME/nginx/default /etc/nginx/sites-available/default 
#sudo /etc/init.d/nginx reload
sudo /etc/init.d/nginx start


sudo chown -R $USERNAME:www-data /var/www
sudo chmod g+rw -R /var/www 
sudo chmod g+s -R /var/www
sudo usermod -a -G www-data $USERNAME


#installing mysql
echo "mysql-server mysql-server/root_password password $PASSWORD" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $PASSWORD" | sudo debconf-set-selections
sudo apt-get install -y mysql-server mysql-client php-mysql || (echo "MySQL Install Failed. Aborting..." && exit 1)


#create database and tables
#TODO: create database and tables
sudo mysql -uroot -p$PASSWORD -e "CREATE DATABASE IF NOT EXISTS $PROJECTNAME" || (echo "Creating Database Failed. Aborting..." && exit 1)

#create "auto" table TODO: make each 'en' default to 0
# sudo mysql -uroot -p$PASSWORD -e 'CREATE TABLE IF NOT EXISTS `pins`.`auto` ( `id` int(11) NOT NULL AUTO_INCREMENT, `master_enable` tinyint(1) NOT NULL, `monday_en` tinyint(1) NOT NULL, `tuesday_en` tinyint(1) NOT NULL, `wednesday_en` tinyint(1) NOT NULL, `thursday_en` tinyint(1) NOT NULL, `friday_en` tinyint(1) NOT NULL, `saturday_en` tinyint(1) NOT NULL, `sunday_en` tinyint(1) NOT NULL, `start_time` time NOT NULL, `end_time` time NOT NULL, `relay` int(11) NOT NULL, `notes` varchar(255), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1;' || (echo "Create 'auto' table Failed. Aborting..." && exit 1) 

#create "manual" table
# sudo mysql -uroot -p$PASSWORD -e 'CREATE TABLE IF NOT EXISTS `pins`.`manual` ( `id` int(11) NOT NULL AUTO_INCREMENT, `mode` varchar(20) NOT NULL, `end_time` time NOT NULL, `relay` int(11) NOT NULL, `notes` varchar(255), PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1;' || (echo "Create 'manual' table Failed. Aborting..." && exit 1) 



#install the laravel api and configure
#sudo rm -R /var/www/laravel #remove previous api
#sudo cp -rf /home/$USERNAME/$PROJECTNAME/api /var/www/laravel #copy new api to previous spot

#create a new env file and configure
sudo cp /var/www/laravel/.env.example /var/www/laravel/.env
sudo sed -i "/DB_DATABASE/ c\DB_DATABASE=$PROJECTNAME" /var/www/laravel/.env
sudo sed -i "/DB_USERNAME/ c\DB_USERNAME=root" /var/www/laravel/.env
sudo sed -i "/DB_PASSWORD/ c\DB_PASSWORD=$PASSWORD" /var/www/laravel/.env


#install composer
sudo curl -sS https://getcomposer.org/installer | /usr/bin/php
sudo /home/$USERNAME/composer.phar global require "laravel/installer"


cd /var/www/laravel
sudo /home/$USERNAME/composer.phar install 
sudo php artisan migrate:refresh --seed
sudo php artisan key:generate 
cd /home/$USERNAME
sudo /etc/init.d/nginx restart


#install phpmyadmin
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password $PASSWORD" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password $PASSWORD" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password $PASSWORD" | sudo debconf-set-selections
#echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none" | sudo debconf-set-selections
sudo apt-get install -y phpmyadmin || (echo "PHPMyAdmin Install Failed. Aborting..." && exit 1)


echo "BACKEND INSTALLATION COMPLETE. PLEASE REBOOT.... (sudo reboot)"

exit 0
